package com.saianfu.common.api;

public class ApiRequest {
    /**
     * 时间戳
     */
    private long timestamp;
    /**
     * 请求方法
     */
    private String method;

    /**
     * 当前登录中的用户的userToken
     */
    private String userToken;
    /**
     * 数据合法性校验签名
     */
    private String sign;
    /**
     * 终端程序版本、前端程序版本
     */
    private String version;
    /**
     * 请求唯一ID，在重复请求时需要忽略，一般在内存缓存中记录
     */
    private String requestUuid;


    /**
     * 终端信息：ip地址，版本（浏览器，手机版本）,型号（浏览器名称，手机型号）
     */
    private String cleintInfo;

    public String getRequestUuid() {
        return requestUuid;
    }

    public void setRequestUuid(String requestUuid) {
        this.requestUuid = requestUuid;
    }

    public String getUserToken() {
        return userToken;
    }

    public void setUserToken(String userToken) {
        this.userToken = userToken;
    }

    public String getCleintInfo() {
        return cleintInfo;
    }

    public void setCleintInfo(String cleintInfo) {
        this.cleintInfo = cleintInfo;
    }

    public long getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(long timestamp) {
        this.timestamp = timestamp;
    }

    public String getMethod() {
        return method;
    }

    public void setMethod(String method) {
        this.method = method;
    }

    public String getSign() {
        return sign;
    }

    public void setSign(String sign) {
        this.sign = sign;
    }

    public String getVersion() {
        return version;
    }

    public void setVersion(String version) {
        this.version = version;
    }
}
