package com.saianfu.common.api;


// "API请求结果")
public class ApiResponse<T> {
    //(value = "请求成功或失败时的数据")
    private T data;

    //(value = "请求失败的原因")
    private String message;

    //(value = "服务器时间")
    private long timestamp;

    //(value = "请求失败的错误编码")
    private int statusCode;

    public ApiResponse() {
        this.timestamp = System.currentTimeMillis();
    }

    public ApiResponse ok(T data) {
        ApiResponse<T> api = new ApiResponse<>();
        api.message = null;
        api.timestamp = System.currentTimeMillis();
        api.statusCode = 200;
        api.data = data;
        return api;
    }

    private ApiResponse fail(int statusCode, String message, T data) {
        ApiResponse<T> api = new ApiResponse<>();
        api.message = message;
        api.timestamp = System.currentTimeMillis();
        api.statusCode = statusCode;
        api.data = data;
        return api;
    }

    public String getMessage() {
        return message;
    }

    public T getData() {
        return data;
    }

    public long getTimestamp() {
        return timestamp;
    }

    public int getStatusCode() {
        return statusCode;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public void setTimestamp(long timestamp) {
        this.timestamp = timestamp;
    }

    public void setStatusCode(int statusCode) {
        this.statusCode = statusCode;
    }

    public void setData(T data) {
        this.data = data;
    }
}
