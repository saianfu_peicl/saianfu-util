package com.saianfu.common.utils

/**
 * 参数校验
 */
object ParameterUtil {

    /**
     * 判断参数为空时，若为空则返回ture，不为空返回false
     */
    fun <T> checkNull(data: T?): Boolean {
        val dataNotNull = data ?: return true
        if (dataNotNull is String && dataNotNull.isEmpty()) {
            return true
        }
        return false
    }

    /**
     * 判断参数为空时，若不为空则返回ture，为空返回false
     */
    fun <T> checkNotNull(data: T?): Boolean {
        if (checkNull(data)) return false
        return true
    }

    /**
     * 判断参数为空时，返回错误提示信息
     */
    fun <T> checkNull(data: T?, errorMsg: String): String? {
        if (checkNull(data)) return errorMsg
        return null
    }

    /**
     * 判断参数列表是否为空，如果未空则提示相应的错误信息，返回第一次遇到为空的参数的错误信息
     */
    fun <T> checkNull(vararg dataAndErrorMsg: Pair<T?, String>): String? {
        dataAndErrorMsg.forEach { pairData ->
            checkNull(pairData.first, pairData.second)?.let {
                return it
            }
        }
        return null
    }

    /**
     * 判断值是否在值列表中，true表示在，false表示不在
     */
    fun <T> checkInValueList(data: T, valueList: List<T>, comparator: Comparator<T>? = null): Boolean {
        if ((data is Number || data is String)) {
            return valueList.contains(data)
        }
        if (comparator == null) {
            throw Exception("复杂数据类型，Comparator不能为空")
        }
        valueList.forEach { dataInList ->
            if (comparator.compare(data, dataInList) == 0) return true
        }
        return false
    }

    /**
     * 判断值是否在值列表中,不在则返回错误信息，若在则返回null
     */
    fun <T> checkInValueList(data: T, valueList: List<T>, errorMsg: String, comparator: Comparator<T>? = null): String? {
        if (checkInValueList(data, valueList, comparator)) return null
        return errorMsg
    }
}