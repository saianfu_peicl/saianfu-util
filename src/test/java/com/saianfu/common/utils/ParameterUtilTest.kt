package com.saianfu.common.utils

import org.junit.Test

import org.junit.Assert.*

class ParameterUtilTest {

    @Test
    fun checkNull() {
        val notNullInt: Int = 34
        assert(!ParameterUtil.checkNull(notNullInt))
        val nullInt: Int? = null
        assert(ParameterUtil.checkNull(nullInt))

        val notNullString: String = "test"
        assert(!ParameterUtil.checkNull(notNullString))
        val nullString: String? = null
        assert(ParameterUtil.checkNull(nullString))

        val newList = listOf<Int>()
        assert(!ParameterUtil.checkNull(newList))
        val nullList: List<Int>? = null
        assert(ParameterUtil.checkNull(nullList))

        assertNull(ParameterUtil.checkNull(notNullInt, "数字不能为空"))
        assertEquals(ParameterUtil.checkNull(nullInt, "数字不能为空"), "数字不能为空")

        assertNull(ParameterUtil.checkNull(
                Pair(notNullInt, "数字不能为空"),
                Pair(notNullString, "字符串不能为空"),
                Pair(newList, "对象不能为空"),
                Pair(notNullInt, "第一个空出现了")
        ))

        assertNotNull(ParameterUtil.checkNull(
                Pair(notNullInt, "数字不能为空"),
                Pair(notNullString, "字符串不能为空"),
                Pair(nullList, "对象不能为空")
        ))
    }

    @Test
    fun checkNotNull() {
        val notNullInt: Int = 34
        assert(!ParameterUtil.checkNull(notNullInt))
        val nullInt: Int? = null
        assert(ParameterUtil.checkNull(nullInt))
    }

    @Test
    fun checkInValueList() {
        val testInt = 5
        val listIntIn = listOf<Int>(5, 6, 7, 8)
        val listIntNotIn = listOf<Int>(1, 2, 3, 4)

        assert(ParameterUtil.checkInValueList(testInt, listIntIn))
        assert(!ParameterUtil.checkInValueList(testInt, listIntNotIn))

        val testString = "test"
        val listStringIn = listOf<String>("test", "test2", "test3", "test4")
        val listStringNotIn = listOf<String>("test1", "test2", "test3", "test4")
        assert(ParameterUtil.checkInValueList(testString, listStringIn))
        assert(!ParameterUtil.checkInValueList(testString, listStringNotIn))

        val user = User("test", 20)
        val userListIn = listOf<User>(User("test", 20), User("test", 30), User("test", 40))
        val userListNotIn = listOf<User>(User("test1", 20), User("test1", 30), User("test1", 40))

        try {
            assert(ParameterUtil.checkInValueList(testString, listStringIn))
        } catch (e: Exception) {
            assertEquals(e.message, "复杂数据类型，Comparator不能为空")
        }

        assert(ParameterUtil.checkInValueList(user, userListIn, Comparator { o1, o2 ->
            if (o1.name == o2.name && o1.age == o2.age) return@Comparator 0
            return@Comparator -1
        }))
        assert(!ParameterUtil.checkInValueList(user, userListNotIn, Comparator { o1, o2 ->
            if (o1.name == o2.name && o1.age == o2.age) return@Comparator 0
            return@Comparator -1
        }))

    }

    class User(var name: String, var age: Int)

}