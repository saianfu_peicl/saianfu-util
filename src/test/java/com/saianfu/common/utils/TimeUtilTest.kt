package com.saianfu.common.utils

import org.junit.Test

import org.junit.Assert.*

class TimeUtilTest {
    private val timeMills = 1600335091786
    val dateY2M2D2 = "2020-09-17"
    val dateY2M2D2H2M2S2 = "2020-09-17 17:31:31"

    @Test
    fun timeFormatY4m2d2() {
        assertEquals("timeFormatY4m2d2出现错误", TimeUtil.timeFormatY4m2d2(timeMills), "2020-09-17")
//       assertEquals("timeFormatY4m2d2出现错误",TimeUtil.timeFormatY4m2d2(timeMills),"2020-09-20")
    }

    @Test
    fun timeFormatY4m2d2h2m2s2() {
        assertEquals("timeFormatY4m2d2h2m2s2出现错误", TimeUtil.timeFormatY4m2d2h2m2s2(timeMills), "2020-09-17 17:31:31")
    }

    @Test
    fun timeFormat() {
    }

    @Test
    fun timeMills() {
        assertEquals("timeMills错误", TimeUtil.timeMills("2020-09-17 17:31:31", TimeUtil.y4m2d2h2m2s2), 1600335091000)
    }

    @Test
    fun date() {
    }

    @Test
    fun timeSpan() {
        val endMills = 1600335622328

        assertEquals("timeSpan错误", TimeUtil.timeSpan(0, 4), "0毫秒")
        assertEquals("timeSpan错误", TimeUtil.timeSpan(786, 4), "786毫秒")
        assertEquals("timeSpan错误", TimeUtil.timeSpan((endMills - timeMills), 4), "8分钟50秒542毫秒")

        assertEquals("timeSpan错误", TimeUtil.timeSpan(86400000L * 100L, 4), "100天")
        assertEquals("timeSpan错误", TimeUtil.timeSpan(86400000L * 100L + 3600000L * 5L, 4), "100天5小时")
        assertEquals("timeSpan错误", TimeUtil.timeSpan(86400000L * 100L + 3600000L * 5L + 60000L * 3L, 4), "100天5小时3分钟")
        assertEquals("timeSpan错误", TimeUtil.timeSpan(86400000L * 100L + 3600000L * 5L + 60000L * 3L + 1000L * 55L, 4), "100天5小时3分钟55秒")
        assertEquals("timeSpan错误", TimeUtil.timeSpan(86400000L * 100L + 3600000L * 5L + 60000L * 3L + 1000L * 55L + 456, 4), "100天5小时3分钟55秒456毫秒")

        assertEquals("timeSpan错误", TimeUtil.timeSpan(86400000L * 100L + 3600000L * 5L + 60000L * 3L + 1000L * 55L + 456, 3), "100天5小时3分钟55秒")
        assertEquals("timeSpan错误", TimeUtil.timeSpan(86400000L * 100L + 3600000L * 5L + 60000L * 3L + 1000L * 55L + 456, 2), "100天5小时3分钟")
        assertEquals("timeSpan错误", TimeUtil.timeSpan(86400000L * 100L + 3600000L * 5L + 60000L * 3L + 1000L * 55L + 456, 1), "100天5小时")
        assertEquals("timeSpan错误", TimeUtil.timeSpan(86400000L * 100L + 3600000L * 5L + 60000L * 3L + 1000L * 55L + 456, 0), "100天")
        assertEquals("timeSpan错误", TimeUtil.timeSpan(86400000L * 100L + 3600000L * 5L + 60000L * 3L + 1000L * 55L + 456, -1), "")

        println(System.currentTimeMillis())
    }
}